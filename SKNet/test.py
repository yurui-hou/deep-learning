import torch


def test(net, test_loader, criterion, writer, args, epoch, loss_vector, accuracy_vector):
    net.eval()
    val_loss, correct = 0, 0
    for index, (data, target) in enumerate(test_loader):
        data = data.to(args.device)
        target = target.to(args.device)
        output = net(data)
        val_loss += criterion(output, target.to(args.device)).data.item()
        pred = output.data.max(1)[1]
        correct += pred.eq(target.data).cpu().sum()
    val_loss /= len(test_loader)
    loss_vector.append(val_loss)
    writer.add_scalar("loss/validation", val_loss, epoch)
    accuracy = 100. * correct.to(torch.float32) / len(test_loader.dataset)
    accuracy_vector.append(accuracy)
    writer.add_scalar("accuracy/validation", accuracy, epoch)

    print("***** Eval results *****")
    print('epoch: {}, Validation set: Average loss: {:.4f}, Accuracy: {}/{} ({:.4f}%)\n'.format(
        epoch, val_loss, correct, len(test_loader.dataset), accuracy))

    return correct,val_loss
