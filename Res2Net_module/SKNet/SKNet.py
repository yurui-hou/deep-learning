import torch
from torch import nn
import torch
import math


class SKConv(nn.Module):
    def __init__(self, features, M=3, G=32, r=16, stride=1, L=32):
        """ Constructor
        Args:
            features: input channel dimensionality.
            M: the number of branchs.
            G: num of convolution groups.
            r: the ratio for compute d, the length of z.
            stride: stride, default 1.
            L: the minimum dim of the vector z in paper, default 32.
        """
        super(SKConv, self).__init__()
        d = max(int(features/r), L)
        self.M = M
        self.features = features
        self.convs = nn.ModuleList([])
        for i in range(M):
            self.convs.append(nn.Sequential(
                nn.Conv2d(features, features, kernel_size=3, stride=stride,
                          padding=1+i, dilation=1+i, groups=G, bias=False),
                nn.BatchNorm2d(features),
                nn.ReLU(inplace=True)
            ))
        self.gap = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Sequential(nn.Conv2d(features, d, kernel_size=1, stride=1, bias=False),
                                nn.BatchNorm2d(d),
                                nn.ReLU(inplace=True))
        self.fcs = nn.ModuleList([])
        for i in range(M):
            self.fcs.append(
                nn.Conv2d(d, features, kernel_size=1, stride=1)
            )
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        batch_size = x.shape[0]

        feats = [conv(x) for conv in self.convs]
        feats = torch.cat(feats, dim=1)
        feats = feats.view(batch_size, self.M, self.features,
                           feats.shape[2], feats.shape[3])

        feats_U = torch.sum(feats, dim=1)
        feats_S = self.gap(feats_U)
        feats_Z = self.fc(feats_S)

        attention_vectors = [fc(feats_Z) for fc in self.fcs]
        attention_vectors = torch.cat(attention_vectors, dim=1)
        attention_vectors = attention_vectors.view(
            batch_size, self.M, self.features, 1, 1)
        attention_vectors = self.softmax(attention_vectors)

        feats_V = torch.sum(feats*attention_vectors, dim=1)

        return feats_V


class SKUnit(nn.Module):
    def __init__(self, in_features, mid_features, out_features, M=3, G=8, r=16, stride=1, L=8,
                 scale=4, stype="normal"):
        """ Constructor
        Args:
            in_features: input channel dimensionality.
            out_features: output channel dimensionality.
            M: the number of branchs.
            G: num of convolution groups.
            r: the ratio for compute d, the length of z.
            mid_features: the channle dim of the middle conv with stride not 1, default out_features/2.
            stride: stride.
            L: the minimum dim of the vector z in paper.
        """
        super(SKUnit, self).__init__()

        self.scale = scale
        self.stype = stype
        self.width = int(math.floor(mid_features / scale))
        if self.scale == 1:
            self.nums = 1
        else:
            self.nums = self.scale - 1
        if stype == 'stage':
            self.pool = nn.AvgPool2d(kernel_size=3, stride=stride, padding=1)

        self.conv1 = nn.Sequential(
            nn.Conv2d(in_features, mid_features, 1, stride=1, bias=False),
            nn.BatchNorm2d(mid_features),
            nn.ReLU(inplace=True)
        )

        conv2_sk = []
        bns = []
        for i in range(self.nums):
            # 输入通道等于输出通道的卷积操作(3*3)
            conv2_sk.append(SKConv(self.width, M=M, G=G, r=r, stride=stride, L=L))
            bns.append(nn.BatchNorm2d(self.width))
        self.convs = nn.ModuleList(conv2_sk)  # https://zhuanlan.zhihu.com/p/75206669
        self.bns = nn.ModuleList(bns)
        self.relu = nn.ReLU(inplace=True)

        self.conv3 = nn.Sequential(
            nn.Conv2d(mid_features, out_features, 1, stride=1, bias=False),
            nn.BatchNorm2d(out_features)
        )
        if in_features == out_features and stride == 1:  # when dim not change, input_features could be added diectly to out
            self.shortcut = nn.Sequential()
        else:  # when dim not change, input_features should also change dim to be added to out
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_features, out_features, kernel_size=1,
                          stride=stride, bias=False),
                nn.BatchNorm2d(out_features)
            )

        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        residual = x

        out = self.conv1(x)

        spx = torch.split(out, self.width, 1)  # 将tensor中第1维度，也即(N,C,H,W)中的C进行拆分
        for i in range(self.nums):
            if i == 0 or self.stype == "stage":
                sp = spx[i]
            else:
                sp = sp + spx[i]
            sp = self.convs[i](sp)
            sp = self.relu(self.bns[i](sp))
            if i == 0:
                out = sp
            else:
                out = torch.cat((out, sp), 1)
        # 注意有两种情况，stage类型来说，这个其实是总是执行sp = spx[i]这一分支，然后进行conv,bn,relu组合，期间执行conv的stride=2的降维，
        # 最后一个splited分支使用pooling 降维
        if self.scale != 1 and self.stype == "normal":
            out = torch.cat((out, spx[self.nums]), 1)
        elif self.scale != 1 and self.stype == "stage":
            out = torch.cat((out, self.pool(spx[self.nums])), 1)

        out = self.conv3(out)

        return self.relu(out + self.shortcut(residual))


class SKNet(nn.Module):
    def __init__(self, class_num, nums_block_list=None, strides_list=None):
        super(SKNet, self).__init__()
        if nums_block_list is None:
            nums_block_list = [2, 2, 2, 2]
        if strides_list is None:
            strides_list = [1, 2, 2, 2]
        self.basic_conv = nn.Sequential(
            nn.Conv2d(3, 32, kernel_size=3, padding=1, stride=1, bias=False),
            nn.BatchNorm2d(32),
            nn.ReLU(inplace=True),
        )
        self.maxpool = nn.MaxPool2d(3, 2, 1)

        self.stage_1 = self._make_layer(
            32, 64, 64, nums_block=nums_block_list[0], stride=strides_list[0], stype="stage")
        self.stage_2 = self._make_layer(
            64, 128, 256, nums_block=nums_block_list[1], stride=strides_list[1])
        self.stage_3 = self._make_layer(
            128, 128, 256, nums_block=nums_block_list[2], stride=strides_list[2])
        self.stage_4 = self._make_layer(
            256, 256, 512, nums_block=nums_block_list[3], stride=strides_list[3])

        self.gap = nn.AdaptiveAvgPool2d((1, 1))
        self.classifier = nn.Linear(256, class_num)

    def _make_layer(self, in_feats, mid_feats, out_feats, nums_block, stride=1, stype="normal"):
        layers = []
        layers.append(SKUnit(in_feats, mid_feats, out_feats, stride=stride, stype="stage"))

        for _ in range(1, nums_block):
            layers.append(SKUnit(out_feats, mid_feats, out_feats))
        return nn.Sequential(*layers)

    def forward(self, x):
        fea = self.basic_conv(x)
        fea = self.maxpool(fea)

        fea = self.stage_1(fea)
        fea = self.stage_2(fea)
        # fea = self.stage_4(fea)

        fea = self.gap(fea)
        fea = torch.squeeze(fea)
        fea = self.classifier(fea)
        return fea


def SKNet26(nums_class=100):
    return SKNet(nums_class, [4, 3, 3, 2])


def SKNet50(nums_class=100):
    return SKNet(nums_class, [3, 4, 6, 3])


def SKNet101(nums_class=100):
    return SKNet(nums_class, [3, 4, 23, 3])
