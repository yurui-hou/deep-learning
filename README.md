# Deep Learning

#### 介绍
*NKUCS2023 COSC0054 Final*
**Cifar-100 classification**

#### 说明
包含了本组实现的所有模型及代码
- ResNet
- SKNet
- Fca-Net
- CA-ResNet
- Fca-ResNet
- CA-SKNet


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request



